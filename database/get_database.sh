#!/bin/bash

start=$(date +%Y-%m-%d-"%T")
##### please, introduce the first and last phenotype_id you want to run when the script is called #####
#for phen in $(seq 0 511)
for phen in $(seq $1 $2)
do
    # header of the "database_"$phen".txt" output file
    echo "org_id" "seed" "viable" "genome_seq" "genome_length" "genome_length_exec" "generation_time" "phen_id" "phen_binary" "equ" "xor" "nor" "andn" "or" "orn" "and" "nand" "not" "transcript_seq" "transcript_length" "tandem_seq" "tandem_length" "tandem_rep" > "database_"$phen".txt"


    #####################################################################################################
    ### get the genotype of 1000 organisms encoding the same phenotype and write the analyze.cfg file ###
    #####################################################################################################

    echo "creating analyze.cfg file ..."
    echo "PURGE_BATCH" > "analyze_"$phen".cfg"
    while read sequence
    do
        echo "LOAD_SEQUENCE" $sequence >> "analyze_"$phen".cfg"
    done < "seed/seed_"$phen".txt"
    echo "RECALC use_random_inputs" >> "analyze_"$phen".cfg"
    echo "TRACE archive 0 -1 1" >> "analyze_"$phen".cfg"
    echo "DETAIL output.txt viable sequence task.8 task.7 task.6 task.5 task.4 task.3 task.2 task.1 task.0 exe_length gest_time task_list" >> "analyze_"$phen".cfg"

    #############################################################################
    ### compute their phenotypes and transcriptomes in different environments ###
    #############################################################################

    for rep in $(seq 1 1000)
    do
        echo "testing 1000 organisms (phenotype: "$phen" |  environment: "$rep")"
    ./avida -a -set RANDOM_SEED $rep -set DATA_DIR "data_"$phen -set ANALYZE_FILE "analyze_"$phen".cfg" -set COPY_MUT_PROB 0 -set DIVIDE_INS_PROB 0 -set DIVIDE_DEL_PROB 0 -set OFFSPRING_SIZE_RANGE 1 -set MIN_COPIED_LINES 0 -set MIN_EXE_LINES 0 -set REQUIRE_EXACT_COPY 1 -set STERILIZE_UNSTABLE 1 -set DEATH_METHOD 0 -set TEST_CPU_TIME_MOD 30 > /dev/null 2>&1
        # remove the headers of the "data_"$phen"/output.txt" file
        sed -i -e 1,19d "data_"$phen"/output.txt"
        # get data in a single file ("data_"$phen"/tmp_1.txt")
        awk -v var=$rep '{print var, $0}' "data_"$phen"/output.txt" >> "data_"$phen"/tmp_1.txt"

        ###
        # get phenotype_id (decimal number)
        ###
        list_tasks=$(awk '{print $15}' "data_"$phen"/tmp_1.txt")
        for i in $list_tasks
        do
            # revert the list of traits so that it corresponds to the order of the output.txt file
            phenotype_rev=$(echo $i | rev)
            # convert the number from binary to decimal
            phen_decimal=$(echo "ibase=2; $phenotype_rev" | bc)
            echo $phenotype_rev $phen_decimal >> "data_"$phen"/tmp_2.txt"
        done
        # add phenotype_id to the "data_"$phen"/tmp_1.txt" file
        paste -d " " "data_"$phen"/tmp_1.txt" "data_"$phen"/tmp_2.txt" > "data_"$phen"/results_"$rep".txt"
        # remove files no longer needed (to avoid appending more data)
        rm "data_"$phen"/tmp_1.txt"
        rm "data_"$phen"/tmp_2.txt"

        ###
        # get the transcriptome
        ###
        # read the trace file (the execution flow during the replication process) corresponding to each organism
        genome_size=100 # genome size
        i=0
        # for each organism (out of 1000) ...
        while read seed viable sequence discard # only interested in the first three columns (discard the rest)
        do
            i=$(($i+1))
            file="data_"$phen"/archive/org-Seq"$i".trace"
            # get the sequence of letters corresponding to the flow of instructions that have been actually executed
            # be carefull because sometimes it executes no-op instructions located in the genome of the offspring (> $genome_length)
            # because of that, if the length of the transcript < generation_time (from the "data_"$phen"/output.txt") the organism is executing its's offspring instructions (we discard the instructions of the offspring that are executed)
            awk '$2 ~ /IP:| / {print $2, $3}' $file | awk 'BEGIN{FS=":"} {print $1, $2, $3}' | awk -v var=$genome_size '{if($2<var) print $3}' | sed 's/(//' | sed 's/)//' > "data_"$phen"/tmp_3.txt"
            transcript=$(awk 'BEGIN{FS=" "} NR==FNR{a[$1]=$2} NR>FNR{$1=a[$1];print}' OFS='\n' instructions.txt "data_"$phen"/tmp_3.txt" | rs -Tg0)
            # find the longest tandem repeat (length > 1) and get its frequency and sequence
            tandem_repeat=$(mreps-2.1/./mreps_ascii -s $transcript | awk '{FS="->"}{print $2}' | awk '{FS="<"}{print $2}' | awk '{FS=">"}{print $1, $2}' | sed 's/[][]//g' | sort -nrk2 | awk '$1>1 {print $1, $2, $3; exit}' | sed "s/'/ /g" | awk '{for(i=1;i<=NF;i++){if($i~/^[0-9.]+/){$i=sprintf("%0.f",$i)}}}1' | awk '{print $2, $3}')
            tandem_n_rep=$(echo $tandem_repeat | awk '{print $1}') # number of repetitions of the tandem sequence
            tandem_seq=$(echo $tandem_repeat | awk '{print $2}') # tandem sequence
            # when the transcript consists on only one letter, the output is: $1="exp." and $2="repetition"
            if [ "$tandem_seq" == "repetition" ]; then
                # get first letter of the transcript
                rep_letter=$(echo $transcript | head -c 1) 
                echo $(($(($phen*1000))+$i)) $transcript ${#transcript} $rep_letter "1" ${#transcript} >> "data_"$phen"/tmp_4.txt"
            else
                echo $(($(($phen*1000))+$i)) $transcript ${#transcript} $tandem_seq ${#tandem_seq} $tandem_n_rep >> "data_"$phen"/tmp_4.txt"
            fi
        done < "data_"$phen"/results_"$rep".txt"
        cat "data_"$phen"/results_"$rep".txt" >> "data_"$phen"/results.txt"
    done # for each $rep (environment or seed)


    ##############################
    ### organize and save data ###
    ##############################

    paste -d " " "data_"$phen"/results.txt" "data_"$phen"/tmp_4.txt" > "data_"$phen"/tmp_5.txt"
    awk -v var=$genome_size '{print $18, $1, $2, $3, var, $13, $14, $17, $16, $4, $5, $6, $7, $8, $9, $10, $11, $12, $19, $20, $21, $22, $23}' "data_"$phen"/tmp_5.txt" >> "database_"$phen".txt"
    # remove files and folders that are no longer needed
    rm "analyze_"$phen".cfg"
    rm -R "data_"$phen

    #########################
    ### compress the data ###
    #########################

    zip "database_"$phen".zip" "database_"$phen".txt"
    rm "database_"$phen".txt"

done

finish=$(date +%Y-%m-%d-"%T")
echo $start
echo $finish
