/* kmp.c : longest extension functions */

#include <malloc.h>
#include "defs.h"

int *computeLIx (char *pattern, int patLen, int dir, int maxPerBnd);
int *computeLOx (char *pattern, int patLen, int dir, int maxPerBnd);

/* Compute extensions of a pattern INSIDE itself */

/* int *computeLI (char *pattern, int n, int dir) 
{
  int *LI = malloc ((n+2)*sizeof(int));

  int i, j=0, k=2 ;

  while (j <= n-2 && LET_TO_NUM(pattern[dir*(j+2)]) == LET_TO_NUM(pattern[dir*(j+1)]))
    j++ ;

LI[2] = j ;

for (i=3; i<=n; i++)
  {
    int l = k + LI[k] - i;
      
    if ((LI[i-k+1] != l) && (l>=0))
LI[i] = (l > LI[i-k+1] ? LI[i-k+1] : l);  //LI[i-k+1];
    else
{
  j = (0 > l ? 0 : l) ;
  while (j+i <= n && LET_TO_NUM(pattern[dir*(j+i)]) == LET_TO_NUM(pattern[dir*(j+1)])) 
    j++ ;
	  
  LI[i] = j ;
	  k = i ;
}
      printf ("LI[%d] = %d \n", i, LI[i]) ; 
    }
LI[n+1]=0 ;

return LI ;
}
*/

int *computeLI (char *pattern, int n, int dir) 
{
  int *LI = malloc ((n+2)*sizeof(int));

  int i, j=0, k=2 ;

  /* while (j <= n-2 && LET_TO_NUM(pattern[dir*j]) == LET_TO_NUM(pattern[dir*(j+1)]))
   */
  while (j <= n-2 && (pattern[dir*j]) == (pattern[dir*(j+1)]))
    j++ ;

  LI[2] = j ;

  for (i=3; i<=n; i++)
    {
      int l = k + LI[k] - i;
      
      if (LI[i-k+1] < l)
	LI[i] = LI[i-k+1];
      else
	{
	  j = (i >= k+LI[k]) ? 0 : l ;
	  /*	  while (j+i-1 <= n-1 && LET_TO_NUM(pattern[dir*j]) == LET_TO_NUM(pattern[dir*(j+i-1)])) */ 
	    while (j+i-1 <= n-1 && (pattern[dir*j]) == (pattern[dir*(j+i-1)])) 
	    j++ ;
	  
	  LI[i] = j ;
	  k = i ;
	}
//      printf ("LI[%d] = %d \n", i, LI[i]) ; 
    }
  LI[n+1]=0 ;

  return LI ;
}



/* Compute extensions of a pattern OUTSIDE, for a given text */

int *computeLO (char *pattern, char *text, int m, int n, int dir, int *LIp)
{ 
  int *LO = malloc ((n+1)*sizeof(int));

  int i, j=0, k=1 ;

  /*  while (j <= m-1 && LET_TO_NUM(pattern[dir*j]) == LET_TO_NUM(text[dir*j])) */
 while (j <= m-1 && (pattern[dir*j]) == (text[dir*j]))
    j++ ;

  LO[1] = j ;

  for (i=2; i<=n; i++)
    {
      int l = k + LO[k] - i ;
      
      if (LIp[i-k+1] < l)
	LO[i] = LIp[i-k+1];
      else
	{
	  j = (i >= k+LO[k]) ? 0 : l ;
	  /*	  while (j<= m-1 && LET_TO_NUM(pattern[dir*j]) == LET_TO_NUM(text[dir*(j+i-1)])) */

	  while (j<= m-1 && (pattern[dir*j]) == (text[dir*(j+i-1)]))
	    j++ ;
	  
	  LO[i] = j ;
	  k = i ;
	}
//      printf ("LO[%d] = %d \n", i, LO[i]) ; 
    }

  return LO ;
}


/* Idem but with restriction for maximal period */
/* Dummy functions O(n^2) */

int *computeLIx (char *pattern, int patLen, int dir, int maxPerBnd) 
     /* Roman's modification of computeLIe */
{
  int *LIx = malloc (maxPerBnd*sizeof(int));

  int i=1 ;

  while(i<maxPerBnd)
    {
      int j = 0 ;
      
      while ( i+j < patLen && 
	      pattern[dir*j] == pattern[dir*(i+j)] )
	j++ ;
      
      LIx[i]=j ;

      i++;
    }

  return LIx ;
}


int *computeLOx (char *pattern, int patLen, int dir, int maxPerBnd) 
     /* Roman's modification of computeLOe */
{
  int *LOx = malloc (maxPerBnd*sizeof(int));

  int i=1 ;

  while(i<maxPerBnd)
    {
      int j = 0 ;
      
      while ( j < patLen && 
	      pattern[dir*j] == pattern[dir*(j-i)] )
	j++ ;
      
      LOx[i]=j ;

      i++;
    }

  return LOx ;
}


/* Idem but with errors */
/* Dummy functions O(n^2) */

/* int *computeLIe (char *pattern, int n, int dir, int epsilon)  */
/* { */
/*   int *LIe = malloc ((n+2)*sizeof(int)); */

/*   int i ; */

/*   for (i=2; i<=n; i++) */
/*     { */
/*       int j = 0 ; */
/*       int psi = epsilon ; */
      
/*       while (i+j <= n &&  */
/* 	     ((pattern[dir*j]) == (pattern[dir*(i+j-1)]) || psi--)) */
/* 	j++ ; */
      
/*       LIe[i]=j ; */
/*     } */

/*   LIe[n+1]=0;  */

/* //  printf ("LI[%d]_{e%d} = %d \n", i, epsilon, LIe[i]) ;  */

/*   return LIe ; */
/* } */

/* int *computeLOe (char *pattern, char *text, int m, int n, */
/* 		 int dir,int epsilon) */
/* {  */
/*   int *LOe = malloc ((n+2)*sizeof(int)); */

/*   int i ; */

/*   for (i=1; i<=n; i++) */
/*     { */
/*       int j = 0 ; */
/*       int psi = epsilon ; */
      
/* 	 while (j <= m-1 &&  */
/* 	     ((pattern[dir*j]) == (text[dir*(i+j-1)]) || psi--)) */
/* 	j++ ; */
      
/*       LOe[i]=j ; */
/*     } */

/* //  printf ("LO[%d]_{e%d} = %d \n", i, epsilon, LOe[i]) ;  */

/*   return LOe ; */
/* } */
