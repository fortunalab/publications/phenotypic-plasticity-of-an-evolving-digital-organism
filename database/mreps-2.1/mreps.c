/* Computing exact repetitions */

#include <stdlib.h>
#include <stdio.h>
#include "defs.h"

extern void print_rep(int rinitpos, int rendpos, int rlength, int rperiod);
extern void finalrepets (int prvFacBgn, int curFacBgn);
extern void SortFirstTypeRepeats(void);
extern void EmergSortFirstTypeRepeats(void);  
extern void FindSecondTypeExactRepeats(void);
void  Showmreps(void);
void print_mrepeat(int rinitpos, int rendpos, int rperiod);
extern void RecodeSeq(int chkLength);
extern sfactorization sfactorize(char *word, int lenght);
extern int *computeLIx(char *pattern, int patLen, int dir, int maxPerBnd);
extern void mainrepets(int prvFacBgn, int curFacBgn, int nxtFacBgn);

extern sfactorization  FactorizeforGDR(void);

extern void guessDepth(int length);

extern int step, LastWindow;
extern int maxPer, dblMinPer;
extern char *seq_working_copy;
extern int FACTYPE;
extern listreps *presortLists, *sortedLists, *maxIntPtr;
extern char  nextLetterCheck, prevLetterCheck ;
extern limits lim ;

extern FILE *output_file;                 // output file (for xml output)
extern char * output_file_name;
extern int xmloutput;

int lenWrd;    // declare it here!
int tooBigReps ;

char *seq_original;   // treated window of original sequence

int verbose = 1;         // defined as extern in defs.h
int verbose_long = 1 ;   // defined as extern in defs.h


void maxreps (char *seq_parameter, int length_parameter)
     /* seaching for exact reps in the current window */
{
  int i;  
  sfactorization sfact ;

  if (length_parameter < dblMinPer)
    return;
  seq_original=seq_parameter; // remember the sequence address in global variable 'seq_original'
  lenWrd=length_parameter;    // remember the sequence length in global variable 'lenWrd'

  VERBL(2) printf("input : '%s'\n", seq_original); 
  VERB(2) printf("length : %d\n", length_parameter);

  /* validating maximal possible period */ 
  maxPer=( (lim.max_period==-1) || (lim.max_period>length_parameter/2) ) ? length_parameter/2 :lim.max_period;
  VERB(1)  printf("Maximal possible period of repeats is %d\n\n", maxPer);

  /* s-factorization (sfact.c) */

#ifdef ACGT

  // create a copy of the sequence to process it in RecodeSeq()
  seq_working_copy = (char *)malloc(length_parameter+2);
  seq_working_copy[0]='\0';
  for (i=0;i<length_parameter;i++) 
    seq_working_copy[i+1]=seq_original[i];
  seq_working_copy[i+1]='\0';
/*   strcpy (seq_working_copy+1, seq_original); */
  RecodeSeq(length_parameter);

  FACTYPE=WITHOUT_EXTRA_SYMBOL;
  guessDepth(length_parameter);
  sfact = FactorizeforGDR();

  free(seq_working_copy);

#else

  sfact = sfactorize (seq_original, length_parameter) ;
  lenWrd=length_parameter;                          // parameter assigned to global variable :-((

#endif

  VERB(1) printf("s-factorization : %d factors\n", sfact->nbfactors);
  
  /* repets of type 1 (mainrepets.c) */

  if( ( presortLists=(listreps *) calloc ((2+lenWrd), sizeof(listreps)) ) == NULL )
    {  
      fprintf(stderr,"Not enough memory for saving the repetitions\n");
      TERMINATE_XML_OUTPUT(output_file,output_file_name,20);
      exit(20);  
    }

  for (i=1; i< sfact -> nbfactors ; i++)
    {

      VERB(1) printf(" s-fact: [%d, %d]\n", 
                     sfact->factor[i], sfact->factor[i+1]) ;
      mainrepets (sfact->factor[i-1], sfact->factor[i], sfact->factor[i+1]); 
	  
    }       
  finalrepets (sfact->factor[i-1], sfact->factor[i]);

  VERB(1) printf("repetitions of type 1 : OK.\n");
  
  /* Now we sort repetitions of type 1 */

  if ( (sortedLists=(listreps *)calloc(lenWrd+2, sizeof(listreps))) )
    SortFirstTypeRepeats();
  else
    EmergSortFirstTypeRepeats();  
 
  VERB(1) printf("sorting m-repets of type 1 : OK.\n");

  VERB(2) printf ("now repetitions of type 2 \n");

  /* repetitions of type 2 */

  FindSecondTypeExactRepeats();

  VERB(1) printf("finding repetitions of type 2 : OK.\n");

  free (sfact);

  /* Now we print the appropriate repetitions */

  Showmreps();

}


void  Showmreps(void)
     /* filtering out exact reps which start in the second half of the window (unless it is the last one),
	or extend beyond the window;
	call print_mrepeat on those reps which go through filtering */
{ 
  int endPstn, initPos, repPer;
  listreps current_rep, *current_pointer, remListRep;

/*   if (sortedLists+step<=maxIntPtr) */
/*   if (step > 0 && sortedLists+step<=maxIntPtr)     // if it is not the last window (?) */
  if (LastWindow==NO)
    maxIntPtr=sortedLists+step-1;                  // output repetition starting in the first half only

  current_pointer=sortedLists;


  if ( (current_rep=(*current_pointer)) )  // if there are reps starting at the first position
                                  // treat them separately (because of prevLetterCheck)
    do
      {  
	repPer=current_rep->rep.period;
	if (seq_original[repPer-1]!=prevLetterCheck) // repeat does not extend to the left
	  {  
	    if ( (endPstn=current_rep->rep.endpos)<lenWrd ) // it does not touch the end
	      print_mrepeat(0, endPstn, repPer);
	    else
	      {  
		if (seq_original[lenWrd-repPer]!=nextLetterCheck)
		  print_mrepeat(0, lenWrd, repPer);
		else
		  tooBigReps=YES;
	      }
	  } 
	else
	  tooBigReps=YES;
	current_rep=(remListRep=current_rep)->next;
	free(remListRep); 
      }  
    while(current_rep);

  while(++current_pointer<=maxIntPtr)     
    {  
      if ((current_rep=(*current_pointer)))
	{  
	  initPos=current_rep->rep.initpos;
	  do
	    {  
	      repPer=current_rep->rep.period;
	      if ( (endPstn=current_rep->rep.endpos)<lenWrd ) // the repeat does not
		// touch the end of the window
		print_mrepeat(initPos, endPstn, repPer);
	      else
		{  
		  if (seq_original[lenWrd-repPer]!=nextLetterCheck)
		    print_mrepeat(initPos, lenWrd, repPer);
		  else
		    tooBigReps=YES;
		}
	      current_rep=(remListRep=current_rep)->next;
	      free(remListRep); 
	    }  
	  while(current_rep);
	}
    }

  free(sortedLists);
}



void print_mrepeat(int rinitpos, int rendpos, int rperiod)
     /* check if the period of the given exact rep is primitive and pass it further for printing 
	to function 'print_rep' (for exe version) or 'printWeb' (for web version)
	(both defined in file printOutput.c) */
{  
  int rlength, j, *LP;

/*   compute the minimal period */
  rlength=rendpos-rinitpos;
  if (lim.min_period>1)
    {  
      LP=computeLIx(seq_original+rinitpos, rlength, 1, rperiod);
      for (j = 1 ; j<rperiod ; j++)
	{  
	  if ( j+LP[j]>=rlength )
            break;
	}
      free(LP);
      if (j>=lim.min_period)
	rperiod=j;
      else
	return;  // throw away the repetition if its minimal period
                 // is smaller than the user-specified minimal period
    }
     

  //This part is for the exe version of the program. Comment it out for web version.

  print_rep(rinitpos+1, rendpos, rlength,  rperiod);


  //This part is for the web version which should be compiled on a SUN machine. Comment it out if used for the exe version.
  //printWeb(start_pstn, rinitpos, rendpos, rlength, rperiod)
   
}

