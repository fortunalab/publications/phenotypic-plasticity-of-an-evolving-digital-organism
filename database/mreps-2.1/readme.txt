http://mreps.univ-mlv.fr/

http://mreps.univ-mlv.fr/howto.html

http://mreps.univ-mlv.fr/tutorial.html


"a tandem repeat is a string repeated contiguously".
" the length of a repeated string is called the period of the repeat, and the number of repeated copies is called the exponent".

"Tandemly repeated DNA (appears in both eukaryotic and prokaryotic genomes) is associated with various regulatory mechanisms and plays an important role in genomic fingerprinting". "For example, tandem repeats participate in protein binding (in eukaryotes)"

In prokaryotes, "a major application of short tandem repeats is based on the inter-individual variability in copy number of certain repeats occurring in single loci. This feature makes tandem repeats a convenient tool for genetic profiling of individuals".

We applied mreps, "a powerful software tool for a fast identification of tandemly repeated structures in DNA sequences".

"mreps is able to identify all types of tandem repeats within a single and fast run on a whole genomic sequence".

"The software is open-source".

"It does not deal with indels (character insertions/deletions), but only with substitutions".

"Importantly, however, the exponent of a repeat is not necessarily an integer number". (we rounded the exponent!!)

"A very old distribution mreps-2.1 is still available. This version can handle the general ascii alphabet, and therefore can still be useful".

"the generic version (mreps_ascii: finds all repetitions in any text (ASCII-character string)) does not allow to search for approximate repetitions".
