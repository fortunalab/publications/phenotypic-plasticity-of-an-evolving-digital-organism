/* sfact.c : computing s-factorisation */
/* this module is used in ASCII version only */

#include <stdlib.h>
#include "defs.h"
#include "dawg.h"

extern node *source; // present in dawg.c

sfactorization s ;

int lvl = 0;

/* calc_leftmost "decorates" the dawg with the \tau-function */

int calc_leftmost (node *dawg)
{
  int a = 0 ;
  int max = -1 ;

  if (dawg -> leftmost >= 0)
    return dawg -> leftmost ;

  lvl++ ;

  for (a=0; a <= MAXCHARS - 1; a++)
    {
      node *current = dawg -> out [a] ;
  
      if (current != UNDEFINED)

	{
	  int c = calc_leftmost (current) ;
	  if (c > max) max = c ;
	}
    }

  lvl-- ;

  dawg -> leftmost = max + 1 ;
  return (max + 1) ;
}


/* Effective computation */

sfactorization sfactorize (char *word, int length)
{
  sfactorization s = malloc (sizeof (struct s_sfactorization)) ;
  int *fac = malloc ((1+ length) * sizeof (int)) ;
  int *pre = malloc (length * sizeof (int)) ;
  int nbf;
  node *dawg ;
  nodelist dawgnodes = NULL ;

  int check ;

  VERB(2) printf("DAWG creation... \n");
  
  source=CREATE_NODE(&dawgnodes);

  dawg = LOAD_KEYWORD(word, length, &dawgnodes) ;


  VERB(1) printf("DAWG creation OK. \n");
  VERB(2) printf("DAWG decoration... \n");

  check = calc_leftmost (source) ;

  VERB(1) printf("DAWG decoration (%d) OK. \n", check);

  {

    node *current = source -> out [LET_TO_NUM(word[0])];
    int pos = 1 ;
    int dpt = 0 ;

//    printf ("current start : %x, %d \n", current, LET_TO_NUM (word[0]) ) ;

    nbf = 1 ;
    fac[0] = 0 ;

    VERB(2) printf("[0] ");

    while (pos <= length-1)
      {

      	node *next = current -> out [LET_TO_NUM(word [pos])] ;

	dpt++ ;

//	printf ("next at : %x \n", next) ;
//	printf ("%d:%d ", pos, next -> leftmost) ;

	if (pos + (next -> leftmost) >= length)    /* Can we continue ? */
	  {
	    current = next ;      /* Yes, we do */
	  }
	else                      /* No : it was the end of a s-factor */
	  {
	    //            printf("length=%d, nbf=%d\n",length,nbf);
	    fac[nbf] = pos ;
	    pre[nbf-1] = (length) - (current -> leftmost) - dpt ;
	    VERB(2) printf("(%d) [%d] ", pre[nbf-1], fac[nbf]) ;
	    current = source -> out [LET_TO_NUM(word [pos])] ;
	    dpt = 0 ;
	    nbf++ ;
	  }
	
	pos++ ;    
      }
    
    if (fac[nbf-1] != pos)       /* The last s-factor */
      {
	dpt++ ;
	//        printf("length=%d, nbf=%d\n",length,nbf);
	fac[nbf] = pos ;
	pre[nbf-1] = (length) - (current -> leftmost) - dpt  ;
	VERB(2) printf("(%d) [%d] ", pre[nbf-1], fac[nbf]) ;
	nbf++ ;
      }

    VERB(2) printf("... OK !\n") ;

    deleteall(dawgnodes);

    VERB(2) printf("DAWG deleted\n") ;

  }  

  /* Now we display the s-factorization */


  VERBL(1)
    {
      int p = 0 ;
      int f = 0 ;
      
      printf("\ns-fact :");
      
      while (p <= length-1)
	{
	  while (p < fac[f])
	    printf("%c", word[p++]) ;
	  printf(" ") ;
	  f++ ;
	} 
      
      printf("\n\n");
    }


  if (0)
  {
    int i ;
    for (i=0; i<=nbf-2; i++)
      {
	int n = fac[i+1] - fac [i] +1 ;
	
	if (n >= 15)
	  printf (" %d ", n) ;
	else
	  printf (".") ;
      }
  }

  s -> factor = fac ;
  s -> previous = pre ;
  s -> nbfactors = --nbf ;

  return s ;
} 
 

