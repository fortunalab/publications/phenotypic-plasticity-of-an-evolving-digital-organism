/* dawg.h : Definitions for the DAWG */
/*************************************/

/* This file contains definitions for the DAWG, 
   used in 'dawg.c' and 'sfact.c'.
   The file is used only in ascii-version */

#define MAXCHARS 256

#define PRIMARY 'P'
#define SECONDARY 'S'
#define UNDEFINED NULL 
#define LEFTMOST_UNDEFINED -1
#define DONTCARE '#'

#define YES 1
#define NO 0

#define ALREADY 'A'
#define NOT_YET 'B'

/* The DAWG structure */

typedef struct NODE {
   struct NODE *out[MAXCHARS];
   char type[MAXCHARS];
   struct NODE *suf_pointer;
   int depth;
   struct KWLIST *terminal;      /* list "terminal" */
   struct NODE *origin;
   int last_letter;
   int number_of_children;       /* comment: we can do without this attribut */
   struct NODE *child;           /* references to the "leftmost" child */
   struct NODE *left_neighbour;  /* "left neighbour" in the children list */
   struct NODE *right_neighbour; /* "right neighbour" in the children list */
   int prefix_degree;
   char visited;              /* auxiliary field; for printing */
   int number;                /* auxiliary field; for numbering */
 /* */
   int leftmost ;  /* auxiliary field for the s-factorization */            
 /* */
} node;


typedef struct KWLIST {       /* element type of the list "terminal" */
   int kw;                    /* pattern for which the node is terminal */
   struct KWLIST *next;       /* next element in the list */
} kwlist;


/* List of the nodes for cleaning */

struct s_nodelist
{
  node *nod ;
  struct s_nodelist *next ;
} ;

typedef struct s_nodelist *nodelist ;

/* Functions of dawg.c */

node *LOAD_KEYWORD(char *v, int length, nodelist *s) ;
node *CREATE_NODE(nodelist *s) ;

void addnode (nodelist *l, node *n);
void deleteall(nodelist s);

