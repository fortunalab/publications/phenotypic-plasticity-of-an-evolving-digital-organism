/* computing approximate repetitions */

/* #include "FndReps.h" */

#include <stdlib.h>
#include "../defs.h"
#include <sys/types.h>
#include <sys/stat.h>

extern limits lim ;
extern char *seq_working_copy, *seq_original;
extern int lenWrd;
/* int *numRep=0;  */         // was never initialized ?? [GK]
extern int maxPer, dblMinPer, actPerBnd  ;
/* extern float minExp; */
extern listreps *sortedLists, *perLists, *initLists, *maxIntPtr; 
/* extern int PRIMEDEPTH; */
extern int FACTYPE;
extern int start_pstn;       // window start position
/* extern int final_pstn; */
/* int numRun , numRun2 ; */
/* extern int noprint; */
extern int step, LastWindow;

extern void guessDepth(int);
extern void print_rep(int,int,int,int);

extern sfactorization FactorizeforGDR(void);
extern void ComputeGlobalFactors(void);
extern void mainstage(void);
extern void SortFirstTypeRepeats(void);
extern void EmergSortFirstTypeRepeats(void);
extern void FindSecondTypeRepeats(void);
extern void SortAllRepeats(void);
extern void EmergSortAllRepeats(void);
extern void FindAllRuns(void);
extern void FindAllStepRuns(void);


void RecodeSeq(int chkLength);
void showByInit(void);


void mismatchProgram(char *seq_parameter, int length_parameter) 
     /* computing approximate reps in the current window */
{
  int i;

  if (length_parameter<dblMinPer)
     return;
  seq_original=seq_parameter; // remember the sequence address in global variable 'seq_original'
  lenWrd=length_parameter;    // remember the sequence length in global variable 'lenWrd'

  /* validating maximal possible period */ 
  maxPer=( (lim.max_period==-1) || (lim.max_period>length_parameter/2) ) ? length_parameter/2 :lim.max_period;
  VERB(1)  printf("Maximal possible period of repeats is %d\n\n", maxPer);

  /* make a copy of the sequence to process it in RecodeSeq() */
  seq_working_copy = (char *)malloc( length_parameter+3);
  seq_working_copy[0]=seq_working_copy[1]='\0';
  seq_working_copy++;
  for (i=0;i<length_parameter;i++) 
    seq_working_copy[i+1]=seq_original[i];
  seq_working_copy[i+1]='\0';
/*   strcpy (seq_working_copy+1, seq_parameter); */
  RecodeSeq(length_parameter);

  guessDepth(length_parameter);
  FACTYPE=WITH_EXTRA_SYMBOL;
  FactorizeforGDR();
  ComputeGlobalFactors();
  mainstage();
  free(--seq_working_copy);
  if ( (sortedLists=(listreps *)calloc(lenWrd+2, sizeof(listreps))) )
    SortFirstTypeRepeats();
  else
    EmergSortFirstTypeRepeats();  
      
  FindSecondTypeRepeats();
  
  if ( (perLists=(listreps *)calloc(actPerBnd, sizeof(listreps))) )
    SortAllRepeats();
  else
    EmergSortAllRepeats();
  
/*   if (step>=lenWrd) */
/*   if (step < 0 || VIRTUAL_STEP >= lenWrd)  // GK */
/*      FindAllRuns(); */
/*   else */
/*      FindAllStepRuns() ; */
  if (LastWindow)
    FindAllRuns();
  else
    FindAllStepRuns() ;
  
  showByInit();  
}

/* void howMany ( ){ */
/*  printf("\n***Total number of global runs is %d ***\n\n", numRun2); */
/* } */

void RecodeSeq(int checkLen)
/* recode sequence 'seq_working_copy' starting from position 1 (!)
   set the global length variable */
{
  char curSmbl;
  int pstn=1;
  
  while((curSmbl=seq_working_copy[pstn])!='\0')
    { switch (curSmbl) 
      {
      case 'a':
      case 'A':
	seq_working_copy[pstn++]=1;
	break;
      case 'c':
      case 'C':
	seq_working_copy[pstn++]=2;
	break;
        case 'g':
      case 'G':
	seq_working_copy[pstn++]=3;
	break;
      case 't':
      case 'T':
	seq_working_copy[pstn++]=4;
	break;
/* 	commented out by GK, 17/10/01 */
/*       case '\n': */
/* 	  seq_working_copy[pstn]=0; */
/* 	  break; */
      default :
	fprintf
	  (stdout, "Error: input sequence has incorrect symbol %c at position %d\n", 
	   curSmbl, start_pstn+pstn);
	exit(-8);
      }
    }
/*   lenWrd=pstn-1; */
  if (pstn-1 != checkLen)
    {  
      fprintf(stdout, "Error: input sequence has special zero symbol at position %d\n",start_pstn+pstn);
      exit(-9);
    }
  VERB(3)  printf("Length of input window : %d\n", lenWrd);
}


/* Output results according to initial position */


void showByInit(void) 
     /* processing the runs stored in the array initLists.
	the runs verifying all parameters are output by function
	'print_rep' */
{  
  int endPstn, initPos, repPer, repLen;
  listreps current, *initPtr, remListRep;
  /*    if (step<=lenWrd-dblMinPer) */
/*   if (step > 0 && VIRTUAL_STEP <= lenWrd-dblMinPer)  // GK */
/*     maxIntPtr=initLists+VIRTUAL_STEP-1; */
/*   else */
/*     maxIntPtr=initLists+lenWrd-dblMinPer; */
  if (LastWindow==NO)
    maxIntPtr=initLists+step-1;
  else 
    maxIntPtr=initLists+lenWrd-dblMinPer;
  for(initPtr=initLists;initPtr<=maxIntPtr;initPtr++)
    {  
      if ((current=(*initPtr)))
	{  
	  initPos=current->rep.initpos+1;
	  do
	    {  
	      endPstn=current->rep.endpos;
	      repLen=endPstn-initPos;
	      repPer=current->rep.period;
	      if ( (repLen>=lim.min_size) && (repLen<=lim.max_size) && ((float)repLen/repPer>=lim.min_exponent) )
		//This part is for the exe version of the program. Comment it out for web version.
		print_rep(initPos, endPstn-1, repLen, repPer);
      
	      //This part is for the web version which should be compiled on a SUN machine. Comment it out if used for the exe version.
	      //printWebMismatch(current);
	      current=(remListRep=current)->next;
	      free(remListRep); 
	    }  
	  while(current);
	}
    }
  free(initLists);
}



/* #ifdef FNDRUNS */
/* void ShowRuns() */
/* {   */
/*   int repPer; */
/*   numRun = 0 ; */
/*   for(repPer=lim.min_period; repPer<actPerBnd; repPer++) */
/*     {   */
/*       if (numRep[repPer]) */
/* 	{  */
/* 	  printf("For period %d there are %d global runs\n", repPer, numRep[repPer]); */
/* 	  numRun+=numRep[repPer]; */
/* 	} */
/*     } */
   
/*   // printf("\n***Total number of global runs is %d ***\n\n", numRun); */
/* } */

/* #else */
/* void ShowGDR() */
/* {   */
/*   int repPer, numGDR=0, curNumGDR; */
/*   listreps curListRep; */
/*   for(repPer=lim.min_period; repPer<actPerBnd; repPer++) */
/*     {   */
/*       if (curListRep=perLists[repPer]) */
/* 	{   */
/* 	  printf("for period %d there are:\n", repPer); */
/* 	  curNumGDR=0; */
/* 	  do */
/* 	    {  curNumGDR++; */
/* #ifdef SHOWGDR */
/* 	    printf( "(%d:%d)\t", curListRep->rep.initpos, curListRep->rep.endpos-1 ); */
/* #endif */
/* 	    }   */
/* 	  while(curListRep=curListRep->next); */
/* #ifdef SHOWGDR */
/* 	  printf("\n"); */
/* #endif */
/* 	  printf("totally %d global-defined repeats:\n", curNumGDR); */
/* 	  numGDR+=curNumGDR; */
/* 	} */
/*     } */
/*   printf("Total number of globally-defined repeats is %d\n", numGDR); */

/* } */
/* #endif */

