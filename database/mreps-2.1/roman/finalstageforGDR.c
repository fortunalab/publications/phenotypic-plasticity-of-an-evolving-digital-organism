/* Search for GDRs of the second type and assembling GDRs into runs */

#include <stdlib.h>
#include "../defs.h"

#define FNDRUNS
void SortFirstTypeRepeats(void);
void EmergSortFirstTypeRepeats(void);
void FindSecondTypeRepeats(void);
void FindSecondTypeExactRepeats(void);
void CopyFactorRepeats(void);
void CopyFactorExactRepeats(void);
void NewSecondTypeRepeat(int repeatPer, int repeatEnd);
void SortAllRepeats(void);
void FindAllRuns(void);
void FindAllStepRuns(void);
int prevHeadPstn, nextHeadPstn, minGapLen, curFact, startRep;
listreps *presortLists;    // repetitions of the first type sorted by end positions
listreps *sortedLists;     // repetitions of both types sorted by start positions
listreps *nextRepPtr, *perLists, *maxIntPtr, *initLists;

extern limits lim;
extern int lenWrd, dblMinPer, maxPer, actPerBnd, tooBigReps;
/* extern int *numRep; */
extern int *copyEnds, *factorEnds, *copyBgns, *factorBgns, numFact;
extern char *seq_original, prevLetterCheck, nextLetterCheck;
extern int step, LastWindow;


void SortFirstTypeRepeats(void)
     /* Sorting reps of the first type (both exact and approximate)
   taken from 'presortLists' (sorted by end positions)
   into 'sortedLists' (sorted by start positions) */
{  
  int endPstn, initPstn, maxEndPstn;
  listreps curListRep, nextListRep;
  maxEndPstn=lenWrd+1;
  for(endPstn=maxEndPstn;endPstn>=dblMinPer;endPstn--)
    {  
      if ((curListRep=presortLists[endPstn]))
	{  
	  do
	    {  
	      initPstn=curListRep->rep.initpos;
	      nextListRep=curListRep->next;
	      curListRep->next=sortedLists[initPstn];
	      sortedLists[initPstn]=curListRep;
	    }  
	  while((curListRep=nextListRep));
	}
    }
  free(presortLists);
  maxIntPtr=sortedLists+(lenWrd-dblMinPer);
}


void EmergSortFirstTypeRepeats(void)
     /* analogous to SortFirstTypeRepeats, for the case when
	there is no enough memory for 'sortedLists' */
{  
  int endPstn, initPstn, maxEndPstn;
  listreps curListRep, nxtListRep, prvListRep, *initPtr;

  sortedLists=presortLists;
  maxEndPstn=lenWrd+1;
  for(endPstn=dblMinPer;endPstn<=maxEndPstn;endPstn++)
    {  
      if ((curListRep=presortLists[endPstn]))
	{  
	  presortLists[endPstn]=NULL;
	  do
	    {  
	      initPstn=curListRep->rep.initpos;
	      nxtListRep=curListRep->next;
	      curListRep->next=sortedLists[initPstn];
	      sortedLists[initPstn]=curListRep;
	    }  
	  while((curListRep=nxtListRep));
	}
    }
  maxIntPtr=sortedLists+(lenWrd-dblMinPer);
  for(initPtr=sortedLists;initPtr<=maxIntPtr;initPtr++)
    {  
      if ((prvListRep=(*initPtr)))
	{  
	  if ((curListRep=prvListRep->next))
	    {  
	      prvListRep->next=NULL;
	      while((nxtListRep=curListRep->next))
		{  
		  curListRep->next=prvListRep;
		  prvListRep=curListRep;
		  curListRep=nxtListRep;
		} 
	      curListRep->next=prvListRep;
	      *initPtr=curListRep;
	    }
	}
    }
}


void FindSecondTypeRepeats(void)
     /* searching for approximate reps of the second type */
{  
  minGapLen=dblMinPer+2;
  nextHeadPstn=1;
  for(curFact=2;curFact<=numFact;curFact++)
    {  
      prevHeadPstn=nextHeadPstn;
      nextHeadPstn=factorEnds[curFact];
      if (nextHeadPstn-prevHeadPstn>minGapLen)
	CopyFactorRepeats();
    }
  free(copyEnds);
  free(factorEnds);
}


void FindSecondTypeExactRepeats(void)
     /* searching for exact reps of the second type */
{  
  minGapLen=dblMinPer+1;
  nextHeadPstn=1;
  for(curFact=1; curFact<numFact; curFact++)
    {  
/*       if ( (prevHeadPstn=nextHeadPstn) >= step ) */
      prevHeadPstn=nextHeadPstn;
      if ( step > 0 && prevHeadPstn >= VIRTUAL_STEP )  // GK
	break;
      nextHeadPstn=factorBgns[curFact+1];
      if (nextHeadPstn-prevHeadPstn>minGapLen)
	CopyFactorExactRepeats();
    }
  free(copyBgns);
  free(factorBgns);
}


void CopyFactorRepeats(void)
     /* computing all approximate reps of the second type, 
	contained in the current LZ-factor in the current window */
{
  listreps copyListRep, initFstRep;
  repetition *copyRep;
  int copyRepEnd, curCopyEnd, shift, maxStart;

  curCopyEnd=copyEnds[curFact];
  shift=nextHeadPstn-curCopyEnd;
  maxStart=nextHeadPstn-minGapLen;
  for(startRep=prevHeadPstn+1;startRep<=maxStart;startRep++)
    {  
      nextRepPtr=sortedLists+startRep;
      if ( (copyListRep=(*(nextRepPtr-shift))) )
	{  
	  initFstRep=(*nextRepPtr);
	  do
	    {  
	      copyRep=&(copyListRep->rep);
	      if ( (copyRepEnd=copyRep->endpos)<curCopyEnd )
		NewSecondTypeRepeat(copyRep->period, copyRepEnd+shift);
	      else
		break;
	    }  
	  while( (copyListRep=copyListRep->next) );
	  *nextRepPtr=initFstRep;
	}
    }
}


void CopyFactorExactRepeats(void)
     /* computing all exact reps of the second type,
	contained in the current s-factor in the current window */
{
  listreps copyListRep, initFstRep;
  repetition *copyRep;
  int copyRepEnd, curCopyEnd, shift, maxStart;

  shift=prevHeadPstn-copyBgns[curFact];
  curCopyEnd=nextHeadPstn-shift;
  maxStart=nextHeadPstn-minGapLen;
  for(startRep=prevHeadPstn+1;startRep<=maxStart;startRep++)
    {  
      nextRepPtr=sortedLists+startRep;
      if ( (copyListRep=(*(nextRepPtr-shift))) )
	{  
	  initFstRep=(*nextRepPtr);
	  do
	    {  
	      copyRep=&(copyListRep->rep);
	      if ( (copyRepEnd=copyRep->endpos)<curCopyEnd )
		NewSecondTypeRepeat(copyRep->period, copyRepEnd+shift);
	      else
		break;
	    }  
	  while( (copyListRep=copyListRep->next) );
	  *nextRepPtr=initFstRep;
	}
    }
}


void NewSecondTypeRepeat(int repeatPer, int repeatEnd)
     /* called from CopyFactorRepeats and CopyFactorExactRepeats;
	processing reps of the second type. 
	Creates for a found rep a corresponding structure 'listreps'
	and inserts it into the corresponding list from 'sortedLists' */
{  
  repetition *newRep;

   if ( (*nextRepPtr=(listreps)calloc(1, sizeof(struct s_listreps)))==NULL )
   {  
     printf("The output is too big.\n");
      printf("Narrow the period bounds.\n");
      exit(15);
   }
   newRep=&((*nextRepPtr)->rep);
   newRep->initpos=startRep;
   newRep->endpos=repeatEnd;
   newRep->period=repeatPer;
   nextRepPtr=&((*nextRepPtr)->next);
}


void SortAllRepeats(void) 
     /* sorting approximate reps taken from 'sortedLists'
	into 'perLists' (array of lists of found reps 
	sorted by their periods) */
{
  int repPer;
  listreps curListRep, nextListRep, *initPtr;
  if (actPerBnd>maxPer+1)
    actPerBnd=maxPer+1;
  for(initPtr=maxIntPtr;initPtr>=sortedLists;initPtr--)
    {  
      if ((curListRep=(*initPtr)))
      {  
	do
	{
	  repPer=curListRep->rep.period;
	  nextListRep=curListRep->next;
	  curListRep->next=perLists[repPer];
	  perLists[repPer]=curListRep;
	}  
	while((curListRep=nextListRep));
      }
    }
  free(sortedLists);
  
}


void EmergSortAllRepeats(void)
     /* analogous to 'SortAllRepeats' in the case when
	there is no enough memory for 'perLists' */
{  
  int repPer;
  listreps curListRep, nextListRep, *initPtr;
  if (actPerBnd>maxPer+1)
    actPerBnd=maxPer+1;
  perLists=sortedLists+lenWrd+1;
  for(initPtr=maxIntPtr;initPtr>=sortedLists;initPtr--)
    {  
      if ((curListRep=(*initPtr)))
	{  
	  *initPtr=NULL;
	  do
	    {  
	      repPer=curListRep->rep.period;
	      nextListRep=curListRep->next;
	      curListRep->next=(*(perLists-repPer));
	      *(perLists-repPer)=curListRep;
	    }  
	  while((curListRep=nextListRep));
	}
    }
  for(repPer=lim.min_period;repPer<actPerBnd;repPer++)
    sortedLists[repPer]=(*(perLists-repPer));
  perLists=(listreps *)realloc(sortedLists, actPerBnd*sizeof(listreps));
}


void FindAllRuns(void)
     /* assembling GDRs stored in 'perLists' into runs,
	and storing these runs into array of lists 'initLists'
	in the case when the current window is the last one
	('initLists' - array of lists of runs of the current window,
	sorted by start positions) */

     /* case where investigated fragment is completely inside the window */
{  
  int repPer, dblPer, *curRepEnd, remRepEnd, repIntPos;
  listreps curListRep, remListRep, nxtListRep;
  if ( (initLists=(listreps *)calloc(lenWrd, sizeof(listreps)))==NULL )
    {  
      printf("Not enough memory for displaying repeats\n");
      exit(4);
    }
  curRepEnd=&remRepEnd;
  for(repPer=actPerBnd-1; repPer>=lim.min_period; repPer--)
    {  
      if ((curListRep=perLists[repPer]))
	{  
	  dblPer=2*repPer;
	  repIntPos=curListRep->rep.initpos;
	  if ( (repIntPos) || (seq_original[repPer-1]!=prevLetterCheck) )
	    {  
	      nxtListRep=curListRep->next;
	      curListRep->next=initLists[repIntPos];
	      initLists[repIntPos]=curListRep;
	      curRepEnd=&(curListRep->rep.endpos);
	      curListRep=nxtListRep;
	    }
	  else
	    {  
	      *curRepEnd=curListRep->rep.endpos;
	      curListRep=(remListRep=curListRep)->next;
	      free(remListRep);
	    }
	  while(curListRep)
	    {  
	      if ( curListRep->rep.initpos<=(*curRepEnd)-dblPer )
		{  
		  *curRepEnd=curListRep->rep.endpos;
		  curListRep=(remListRep=curListRep)->next;
		  free(remListRep);
		}
	      else
		{  
		  repIntPos=curListRep->rep.initpos;
		  nxtListRep=curListRep->next;
		  curListRep->next=initLists[repIntPos];
		  initLists[repIntPos]=curListRep;
		  curRepEnd=&(curListRep->rep.endpos);
		  curListRep=nxtListRep;
		}
	    }
	  curRepEnd=&remRepEnd;
	}
    }
  free(perLists);
}


void FindAllStepRuns(void)
     /* analogous to FindAllRuns for the case when 
	the current window is not the last one */

     /* case where investigated fragment extends beyond the window */
     /* called only if step is specified and smaller than lenWrd */
{  
  int repPer, dblPer, *curRepEnd, remRepEnd, repIntPos;
  listreps curListRep, remListRep, nxtListRep;

  if ( (initLists=(listreps *)calloc(step, sizeof(listreps)))==NULL )
    {  
      printf("Not enough memory for showing the repeats\n");
      exit(4);
    }
  curRepEnd=&remRepEnd;
  for(repPer=actPerBnd-1; repPer>=lim.min_period; repPer--)
    {  
      if ((curListRep=perLists[repPer]))
	{  
	  dblPer=2*repPer;
	  repIntPos=curListRep->rep.initpos;
	  if ( (repIntPos) || (seq_original[repPer-1]!=prevLetterCheck) )
	    {  
	      if (repIntPos<step)
		{  
		  nxtListRep=curListRep->next;
		  curListRep->next=initLists[repIntPos];
		  initLists[repIntPos]=curListRep;
		  curRepEnd=&(curListRep->rep.endpos);
		  curListRep=nxtListRep;
		}
	      else
		{  
		  do
		    {  
		      curListRep=(remListRep=curListRep)->next;
		      free(remListRep);
		    }  
		  while(curListRep);
		  continue;
		}
	    }
	  else
	    {  
	      *curRepEnd=curListRep->rep.endpos;
	      curListRep=(remListRep=curListRep)->next;
	      free(remListRep);
	    }
	  while(curListRep)
	    {  
	      if ( curListRep->rep.initpos<=(*curRepEnd)-dblPer )
		{  
		  *curRepEnd=curListRep->rep.endpos;
		  curListRep=(remListRep=curListRep)->next;
		  free(remListRep);
		}
	      else
		{  
		  if ( (repIntPos=curListRep->rep.initpos)<step )
		    {  
		      nxtListRep=curListRep->next;
		      curListRep->next=initLists[repIntPos];
		      initLists[repIntPos]=curListRep;
		      curRepEnd=&(curListRep->rep.endpos);
		      curListRep=nxtListRep;
		    }
		  else
		    {  
		      do
			{  
			  curListRep=(remListRep=curListRep)->next;
			  free(remListRep);
			}  
		      while(curListRep);
		      break;
		    }
		}
	    }
	  if ( (*curRepEnd>lenWrd) && 
	       (seq_original[lenWrd-repPer]==nextLetterCheck) &&
	       (curRepEnd!=&remRepEnd) )
	    {  
	      initLists[repIntPos]=(remListRep=initLists[repIntPos])->next;
	      free(remListRep);
	      tooBigReps=YES;
	    }
	  curRepEnd=&remRepEnd;
	}
    }
  free(perLists);
}

