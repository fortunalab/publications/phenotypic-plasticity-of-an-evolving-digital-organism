/* dawg.c : Computing the DAWG */
/*******************************/

/* This module was originally written by G. Kucherov
   (www.loria.fr/~kucherov) for his 'grappe' program.
   It has been modified and simplified for mreps.
   It computes the DAWG of a string in linear time.  
   It is used in ASCII version only. */

#include <stdio.h>
#include <stdlib.h>
#include "dawg.h"
#include "defs.h"

node *source;

/*Declarations */

node *SPLIT(node *, int , nodelist *);
void SET_SUF_POINTER(node *, node *);
void DELETE_SUF_POINTER(node *, node *);
void DELETE_NODE(node *);

/* Functions */

node *APPEND_LETTER(node*activenode, int letter, nodelist *s)
{
   node *newactivenode,*varnode,*SPLIT(),*CREATE_NODE(nodelist *s);
  
   if ((newactivenode=activenode->out[letter]) != UNDEFINED)
     {
       if (activenode->type[letter] == PRIMARY) return(newactivenode);
       else return(SPLIT(activenode,letter,s));
     }
   else 
     {
       if ((newactivenode = CREATE_NODE(s)) == NULL)
	 {
	   fprintf(stderr,"Not enough memory!");
	   return NULL;
	 }
       activenode->out[letter] = newactivenode; 
       activenode->type[letter] = PRIMARY;

       newactivenode->origin = activenode;
       newactivenode->last_letter = letter;
       newactivenode->depth = activenode->depth + 1;

       varnode = activenode->suf_pointer;

       while (varnode != UNDEFINED && varnode->out[letter]==UNDEFINED)
	 {
	   varnode->out[letter] = newactivenode;
	   varnode->type[letter] = SECONDARY;
	   varnode = varnode->suf_pointer;
	 }

       SET_SUF_POINTER(newactivenode,
		       (varnode == UNDEFINED) ? source
		       : ((varnode->type[letter] == PRIMARY) ?
			  varnode->out[letter] : SPLIT(varnode,letter,s)));

       return(newactivenode);
     }
}

/* modified version: the second parameter is a and not targetnode
     because SPLIT is always called SPLIT(x,x->out[a]) */
node *SPLIT(node *originnode, int a, nodelist *s)
{
  node *targetnode,*newtargetnode,*n,*varnode,*CREATE_NODE(nodelist *s);
  int appendedletter,c;
  
  targetnode = originnode->out[a];
  newtargetnode = CREATE_NODE(s);
  appendedletter = targetnode->last_letter;
  originnode->out[a] = newtargetnode; originnode->type[a] = PRIMARY;
  newtargetnode->origin = originnode;
  newtargetnode->last_letter = appendedletter;
  newtargetnode->depth = originnode->depth + 1;

  /* additional instructions */
 //  if (currentnode == targetnode) 
 //     if (length<=newtargetnode->depth) currentnode=newtargetnode; 
 /* end of additional instructions */

  for (c=0;c<MAXCHARS;c++)
    if ((n=targetnode->out[c]) != UNDEFINED) 
      {
	newtargetnode->out[c] = n; 
	newtargetnode->type[c] = SECONDARY;
      }
  
  SET_SUF_POINTER(newtargetnode,targetnode->suf_pointer);
  DELETE_SUF_POINTER(targetnode,targetnode->suf_pointer);
  SET_SUF_POINTER(targetnode,newtargetnode);
 
/* after this: */
/* targetnode has the suffix pointer to newtargetnode, */
/* targetnode is the only child of newtargetnode, */

   varnode = originnode->suf_pointer;
 
   while (varnode != UNDEFINED && varnode->type[appendedletter]==SECONDARY)
     {
       varnode->out[appendedletter] = newtargetnode;
       varnode = varnode->suf_pointer;
     }
  
   return(newtargetnode);
}



void SET_SUF_POINTER(node *newchild, node *father)
  /* it is supposed that newchild does not have a suffix pointer, */
  /* that is, does not belong to any child list. child is inserted */
  /* in the father's list at the leftmost position */
{
  node *current_leftmost_child;
  if (father->child !=UNDEFINED)
    {
      /* unnecessary test */
/*       if (father->number_of_children == 0 ) printf("problem5...\n"); */
      /* end of unnecessary test */
      current_leftmost_child=father->child;
      newchild->right_neighbour = current_leftmost_child;
      current_leftmost_child->left_neighbour = newchild;
    }
  newchild->suf_pointer=father;
  father->child=newchild;
  father->number_of_children++;
  return;
}

void DELETE_SUF_POINTER(node * oldchild,node * father)
    {
  if (oldchild->left_neighbour==UNDEFINED)
    if (oldchild->right_neighbour != UNDEFINED)
      /* oldchild is not the only child of father but is the leftmost one */
      {
	/* unnecessary test */
/* 	if (father->number_of_children <= 1) printf("problem6 ...\n"); */
	/* end of unnecessary test */
	oldchild->right_neighbour->left_neighbour = UNDEFINED;
	father->child = oldchild->right_neighbour;
	oldchild->right_neighbour = UNDEFINED;
      }
    else
      /* oldchild is the only child of father */
      {
/* unnecessary test */
/* 	if (father->number_of_children != 1) printf("problem7 ...\n"); */
	/* end of unnecessary test */
	father->child = UNDEFINED; 
         } 
  else
      if (oldchild->right_neighbour != UNDEFINED)
	/* oldchild has both right and left neighbour */
         {
	   /* unnecessary test */
/* 	   if (father->number_of_children <= 1) printf("problem6 ...\n"); */
/* end of unnecessary test */
	   oldchild->right_neighbour->left_neighbour = oldchild->left_neighbour;
	   oldchild->left_neighbour->right_neighbour = oldchild->right_neighbour;
	 oldchild->left_neighbour = oldchild->right_neighbour = UNDEFINED;
         }
      else
	/* oldchild is not the only child of father but is the rightmost one */
	{
	  /* unnecessary test */
/* 	  if (father->number_of_children <= 1) printf("problem6 ...\n"); */
	  /* end of unnecessary test */
	  oldchild->left_neighbour->right_neighbour = UNDEFINED;
	  oldchild->left_neighbour = UNDEFINED;
	}
  oldchild->suf_pointer=UNDEFINED;
  father->number_of_children--;
  return;
}


node *LOAD_KEYWORD(char v[], int length, nodelist *s)
{
  int i; 
  node *activenode;
  
  activenode=source;
  
  VERB (1) printf("\n") ;

  for (i=0;i<length;i++) 
    {
      activenode = APPEND_LETTER(activenode, LET_TO_NUM (v[i]), s);
      activenode->prefix_degree++;
    }
  
  VERB (1) printf("\n") ;
  
  return activenode;
}

node *CREATE_NODE(nodelist *s)
{
  node *newnode;
  int i;

  if ((newnode = (node*) malloc(sizeof(node)))== NULL)
    {
      fprintf(stderr,"Not enough memory!");
      return NULL;
    }
      
  addnode (s, newnode);
  // printf("memory allocated, %d\n",sizeof(node)); 
  for(i=0;i<MAXCHARS;i++) newnode->out[i]=UNDEFINED;
  newnode->suf_pointer=newnode->origin=UNDEFINED;
  newnode->child=newnode->left_neighbour=newnode->right_neighbour=UNDEFINED;
  newnode->terminal=NULL;
  newnode->number_of_children=newnode->prefix_degree=0;
  newnode->depth=0;
  newnode->visited=NOT_YET;
  newnode->last_letter=-1;
  newnode->number=0;
/**/
  newnode -> leftmost = LEFTMOST_UNDEFINED ;
/**/
  return newnode;
}

void DELETE_NODE(node *nod)
/* it is supposed that no edges point to nod and that */
/* the suffix pointer of nod has been deleted */
{
  free(nod);
  return;
}


/* Additional functions to clean the DAWG */

void deleteall(nodelist s)
{
  nodelist current = s ;

  while (current)
    {
      nodelist old = current ;
      
      current = current -> next ;

      DELETE_NODE (old -> nod);
      free (old);
    }

  return ;
}


void addnode (nodelist *l, node *n)
{
  nodelist newl;

  newl= malloc (sizeof (struct s_nodelist));
  if (newl == NULL)
    {
      fprintf(stderr,"Not enough memory!");
    }
  newl -> nod = n ;
  newl -> next = *l ;

  *l = newl ;
}
